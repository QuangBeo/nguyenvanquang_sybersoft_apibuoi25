//render todo list

function renderTodoList(todos){
    var contentHTML = "";

    todos.forEach(function(item){
        var content = `
            <tr>
                <td>${item.id}</td>
                <td>${item.name}</td>
                <td>${item.desc}</td>
                <td>
                    <input type="checkbox" ${item.isComplete ? "checked" : ""}  />
                </td>
                <td class="btn btn-danger" onclick="removeTodo(${item.id})">Delete</td>
                <td class="btn btn-secondary" onclick="editTodo(${item.id})">Sửa</td>
            </tr>
            `;
        contentHTML += content;
    });
    document.getElementById("tbody-todos").innerHTML = contentHTML;
}

function turnOnLoadding(){
    document.getElementById("loading").style.display = "flex";
}

function turnOffLoadding(){
    document.getElementById("loading").style.display = "none";
}

//Lấy thông tin từ form
function layThongTinTuForm(){
    var name = document.getElementById('name').value;
    var desc = document.getElementById('desc').value;
    return {
        name: name,
        desc: desc,
    }
}