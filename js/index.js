const BASE_URL = "https://636917d015219b84960f2de7.mockapi.io";
//render all todos servieces
function fetchAllTodo(){
    turnOnLoadding();
    axios({
        url: `${BASE_URL}/uses`,
        method: "GET",
    })
    .then(function(res){
        turnOffLoadding();
        renderTodoList(res.data);
    })
    .catch(function(res){
        turnOffLoadding();
        console.log('res: ', res);
    });
}


//Chạy lần đầu khi load trang
fetchAllTodo();

//Xoá todos

function removeTodo(idTodo){
    turnOnLoadding();
    axios({
        url: `${BASE_URL}/uses/${idTodo}`,
        method: "DELETE",
    })
    .then(function(res){
        turnOffLoadding();
        fetchAllTodo();
    })
    .catch(function(err){
        turnOffLoadding();
        console.log('err: ', err);
    })
}

//add todo

function addTodo(){
    var data = layThongTinTuForm();
    var newTodo = {
        name: data.name,
        desc: data.desc,
        isComplete: true,
    };
    turnOnLoadding();
    axios({
        url: `${BASE_URL}/uses`,
        method: "POST",
        data: newTodo,
    })
    .then(function(res){
        turnOffLoadding();
        fetchAllTodo();
    })
    .catch(function(rerr){
        turnOffLoadding();
    })
}

//sua todos
function editTodo(idTodo){
    turnOnLoadding();
    axios({
        url: `${BASE_URL}/uses/${idTodo}`,
        method: "GET",
    })
    .then(function(res){
        turnOffLoadding();
        document.getElementById("name").value = res.data.name;
        document.getElementById("desc").value = res.data.desc;

        idEdited = res.data.id;
    })
    .catch(function(err){
        turnOffLoadding();
        console.log('err: ', err);
    })
}

//update todo

function updateTodo(){
    let data = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/uses/${idEdited}`,
        method: "PUT",
        data: data,
    })
    .then(function(res){
        fetchAllTodo();
    })
    .catch(function(err){
        turnOffLoadding();
        console.log('err: ', err);
    })
}